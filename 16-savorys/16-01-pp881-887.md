---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->



<div class="main">

# XVI.　セイヴォリー {#savory}

<div class="frchapenv">Savorys</div></div>


\index{せいうおりー@セイヴォリー|(}
\index{けいしよく@軽食⇒セイヴォリー|(}
\index{すなつく@スナック|see セイヴォリー}
\index{savory@savory|(}

</div><!--endMain-->


<div class="recette">

#### アリュメット {#alllumettes}

<div class="frsubenv">Allumettes</div></div>

\index{せいうおりー@セイヴォリー!ありゆめつと@アリュメット}
\index{ありゆめつと@アリュメット}
\index{savory@savory!allumettes@allumettes}
\index{allumets@allumets}


\atoaki{}

#### アンジュ・ア・シュヴァル {#anges-a-cheval}

<div class="frsubenv">Anges à Cheval</div></div>

\index{せいうおりー@セイヴォリー!あんしゆあしゆうある@アンジュアシュヴァル}
\index{あんしゆあしゆうある@アンジュアシュヴァル}
\index{savory@savory!angesacheval@anges à cheval}
\index{angesacheval@anges à cheval}




\atoaki{}


#### ベニエ・スフレ

<div class="frsubenv">Beignets-Souffles</div>

\index{せいうおりー@セイヴォリー!へにえすふれ@ベニエスフレ}
\index{へにえすふれ@ベニエスフレ}
\index{savory@savory!beignetssouffles@beignetssoufflés}
\index{beignetssouffles@beignetssoufflés}



\atoaki{}

#### ブーレック・ア・ラ・テュルク

<div class="frsubenv">Beurrecks-a-la-Turque</div>

\index{せいうおりー@セイヴォリー!ふーれつくあらてゆるく@ブーレックアラテュルク}
\index{ふーれつくあらてゆるく@ブーレックアラテュルク}
\index{savory@savory!beurrecksalaturque@beurrecksàlaturque}
\index{beurrecksalaturque@beurrecksàlaturque}



\atoaki{}

#### ブロシェット・デュイットル・リュシフェル

<div class="frsubenv">Brochettes-d’huitres-Lucifer</div>

\index{せいうおりー@セイヴォリー!ふろしえつとてゆいつとるりゆしふえる@ブロシェットデュイットルリュシフェル}
\index{ふろしえつとてゆいつとるりゆしふえる@ブロシェットデュイットルリュシフェル}
\index{savory@savory!brochettesd’huitreslucifer@brochettesd’huîtreslucifer}
\index{brochettesd’huitreslucifer@brochettesd’huîtreslucifer}





\atoaki{}

#### シュー・オ・フロマージュ

<div class="frsubenv">Choux-au-fromage</div>

\index{せいうおりー@セイヴォリー!しゆーおふろまーしゆ@シューオフロマージュ}
\index{しゆーおふろまーしゆ@シューオフロマージュ}
\index{savory@savory!chouxaufromage@chouxaufromage}
\index{chouxaufromage@chouxaufromage}





\atoaki{}

#### カマンベール・フリット

<div class="frsubenv">Camembert-frit</div>

\index{せいうおりー@セイヴォリー!かまんへーるふりつと@カマンベールフリット}
\index{かまんへーるふりつと@カマンベールフリット}
\index{savory@savory!camembertfrit@camembertfrit}
\index{camembertfrit@camembertfrit}





\atoaki{}

#### カナッペ・オウ・トースト

<div class="frsubenv">Canapes-ou-Toasts</div>

\index{せいうおりー@セイヴォリー!かなつへおうとーすと@カナッペオウトースト}
\index{かなつへおうとーすと@カナッペオウトースト}
\index{savory@savory!canapesoutoasts@canapésoutoasts}
\index{canapesoutoasts@canapésoutoasts}







\atoaki{}

#### カナッペ・カドガン

<div class="frsubenv">Canapes-Cadogan</div>

\index{せいうおりー@セイヴォリー!かなつへかとかん@カナッペカドガン}
\index{かなつへかとかん@カナッペカドガン}
\index{savory@savory!canapescadogan@canapéscadogan}
\index{canapescadogan@canapéscadogan}






\atoaki{}


#### カナッペ・デ・グルメ

<div class="frsubenv">Canapes-des-Gourmets</div>

\index{せいうおりー@セイヴォリー!かなつへてくるめ@カナッペデグルメ}
\index{かなつへてくるめ@カナッペデグルメ}
\index{savory@savory!canapesdesgourmets@canapésdesgourmets}
\index{canapesdesgourmets@canapésdesgourmets}


\atoaki{}

#### カナッペ・アイヴァンホー

<div class="frsubenv">Canapes-Ivanhoe</div>

\index{せいうおりー@セイヴォリー!かなつへあいうあんほー@カナッペアイヴァンホー}
\index{かなつへあいうあんほー@カナッペアイヴァンホー}
\index{savory@savory!canapesivanhoe@canapésivanhoé}
\index{canapesivanhoe@canapésivanhoé}


\atoaki{}

#### カナッペ・ア・レコセーズ

<div class="frsubenv">Canapes-a-l'Ecossaise</div>

\index{せいうおりー@セイヴォリー!かなつへあれこせーす@カナッペアレコセーズ}
\index{かなつへあれこせーす@カナッペアレコセーズ}
\index{savory@savory!canapesal'ecossaise@canapésàl’écossaise}
\index{canapesal'ecossaise@canapésàl’écossaise}


\atoaki{}

#### カルカス・ド・ヴォライユ

<div class="frsubenv">Carcasses-de-volailles</div>

\index{せいうおりー@セイヴォリー!かるかすとうおらいゆ@カルカスドヴォライユ}
\index{かるかすとうおらいゆ@カルカスドヴォライユ}
\index{savory@savory!carcassesdevolailles@carcassesdevolailles}
\index{carcassesdevolailles@carcassesdevolailles}


\atoaki{}

#### シャンピニオン・スー・クローシュ

<div class="frsubenv">Champignons-sous-cloche</div>

\index{せいうおりー@セイヴォリー!しやんひにおんすーくろーしゆ@シャンピニオンスークローシュ}
\index{しやんひにおんすーくろーしゆ@シャンピニオンスークローシュ}
\index{savory@savory!champignonssouscloche@champignonssouscloche}
\index{champignonssouscloche@champignonssouscloche}


\atoaki{}

#### コンデ・オ・フロマージュ

<div class="frsubenv">Condes-au-fromage</div>

\index{せいうおりー@セイヴォリー!こんておふろまーしゆ@コンテオフロマージュ}
\index{こんておふろまーしゆ@コンテオフロマージュ}
\index{savory@savory!condesaufromage@condésaufromage}
\index{condesaufromage@condésaufromage}


\atoaki{}

#### クレーム・フリット・オ・フロマージュ

<div class="frsubenv">Creme-frite-au-fromage</div>

\index{せいうおりー@セイヴォリー!くれーむふりつとおふろまーしゆ@クレームフリットオフロマージュ}
\index{くれーむふりつとおふろまーしゆ@クレームフリットオフロマージュ}
\index{savory@savory!cremefriteaufromage@crèmefriteaufromage}
\index{cremefriteaufromage@crèmefriteaufromage}


\atoaki{}

#### クロケット・ド・カマンベール

<div class="frsubenv">Croquettes-de-Camembert</div>

\index{せいうおりー@セイヴォリー!くろけつととかまんへーる@クロケットドカマンベール}
\index{くろけつととかまんへーる@クロケットドカマンベール}
\index{savory@savory!croquettesdecamembert@croquettesdecamembert}
\index{croquettesdecamembert@croquettesdecamembert}


\atoaki{}

#### デリス・ド・フォアグラ

<div class="frsubenv">Delices-de-Foie-gras</div>

\index{せいうおりー@セイヴォリー!てりすとふおあくら@デリスドフォアグラ}
\index{てりすとふおあくら@デリスドフォアグラ}
\index{savory@savory!delicesdefoiegras@délicesdefoiegras}
\index{delicesdefoiegras@délicesdefoiegras}


\atoaki{}

#### ディアブロタン

<div class="frsubenv">Diablotins</div>

\index{せいうおりー@セイヴォリー!ていあふろたん@ディアブロタン}
\index{ていあふろたん@ディアブロタン}
\index{savory@savory!diablotins@diablotins}
\index{diablotins@diablotins}


\atoaki{}

#### フォンダン・オ・シェステル

<div class="frsubenv">Fondants-au-Chester</div>

\index{せいうおりー@セイヴォリー!ふおんたんおしえすてる@フォンダンオシェステル}
\index{ふおんたんおしえすてる@フォンダンオシェステル}
\index{savory@savory!fondantsauchester@fondantsauchester}
\index{fondantsauchester@fondantsauchester}


\atoaki{}

#### ガレット・ブリアルド {#galettes-briardes}

<div class="frsubenv">Galettes Briardes</div>

\index{せいうおりー@セイヴォリー!かれつとふりあると@ガレットブリアルド}
\index{かれつとふりあると@ガレットブリアルド}
\index{savory@savory!galettesbriardes@galettesbriardes}
\index{galettesbriardes@galettesbriardes}


\atoaki{}

#### レタンス・ア・ラ・ディアブル {#laitance-a-la-diable}

<div class="frsubenv">Laitances à la Diable</div>

\index{せいうおりー@セイヴォリー!れたんすあらていあふる@レタンスアラディアブル}
\index{れたんすあらていあふる@レタンスアラディアブル}
\index{savory@savory!laitancesaladiable@laitancesàladiable}
\index{laitancesaladiable@laitancesàladiable}


\atoaki{}

#### オムレット・ア・レコセイズ {#omlette-a-l-ecossaise}

<div class="frsubenv">Omelette à l'Écossaise</div>

\index{せいうおりー@セイヴォリー!おむれつとあれこせいす@オムレットアレコセイズ}
\index{おむれつとあれこせいす@オムレットアレコセイズ}
\index{savory@savory!omeletteal'ecossaise@omeletteàl'écossaise}
\index{omeletteal'ecossaise@omeletteàl'écossaise}



\atoaki{}

#### オス・グリエ {#os-grilles}

<div class="frsubenv">Os grillés</div>

\index{せいうおりー@セイヴォリー!おすくりえ@オスグリエ}
\index{おすくりえ@オスグリエ}
\index{savory@savory!osgrilles@osgrillés}
\index{osgrilles@osgrillés}


\atoaki{}

#### パイエット・オ・パルメザン {#paillettes-au-parmesan}

<div class="frsubenv">Paillettes au Parmesan</div>

\index{せいうおりー@セイヴォリー!はいえつとおはるめさん@パイエットオパルメザン}
\index{はいえつとおはるめさん@パイエットオパルメザン}
\index{savory@savory!paillettesauparmesan@paillettesauparmesan}
\index{paillettesauparmesan@paillettesauparmesan}


\atoaki{}

#### パヌケ・モスコヴィート {#pannequets-moscovite}

<div class="frsubenv">Pannequets Moscovite</div>

\index{せいうおりー@セイヴォリー!はぬけもすこういーと@パヌケモスコヴィート}
\index{はぬけもすこういーと@パヌケモスコヴィート}
\index{savory@savory!pannequetsmoscovite@pannequetsmoscovite}
\index{pannequetsmoscovite@pannequetsmoscovite}


\atoaki{}

#### プディング・ド・フロマージュ・オ・パン {#pudding-de-fromage-au-pain}

<div class="frsubenv">Pudding de fromage au pain</div>

\index{せいうおりー@セイヴォリー!ふていんくとふろまーしゆおはん@プディングドフロマージュオパン}
\index{ふていんくとふろまーしゆおはん@プディングドフロマージュオパン}
\index{savory@savory!puddingdefromageaupain@Puddingdefromageaupain}
\index{puddingdefromageaupain@Puddingdefromageaupain}


\atoaki{}

#### サルディーヌ・ア・ラ・ディアブル {#sardines-a-la-diable}

<div class="frsubenv">Sardines à la Diable</div>

\index{せいうおりー@セイヴォリー!さるていーぬあらてぃあふる@サルディーヌアラディアブル}
\index{さるていーぬあらてぃあふる@サルディーヌアラディアブル}
\index{savory@savory!sardinesaladiable@sardinesàladiable}
\index{sardinesaladiable@sardinesàladiable}


\atoaki{}

#### スコッチ・ウッドコック {#scotch-woodcock}

<div class="frsubenv">Scotch Woodcock</div>

\index{せいうおりー@セイヴォリー!すこつちうつとこつく@スコッチウッドコック}
\index{すこつちうつとこつく@スコッチウッドコック}
\index{savory@savory!scotchwoodcock@scotchwoodcock}
\index{scotchwoodcock@scotchwoodcock}


\atoaki{}

#### タルトレット・アニェス {#tartelettes-agnes}

<div class="frsubenv">Tartelettes Agnès</div>

\index{せいうおりー@セイヴォリー!たるとれつとあにぇす@タルトレットアニェス}
\index{たるとれつとあにぇす@タルトレットアニェス}
\index{savory@savory!tartelettesagnes@tartelettesagnès}
\index{tartelettesagnes@tartelettesagnès}


\atoaki{}

#### タルトレット・ア・レコセイズ {#tartelettes-a-l-ecossaise}

<div class="frsubenv">Tartelettes a l'Ecossaise</div>

\index{せいうおりー@セイヴォリー!たるとれつとあれこせいす@タルトレットアレコセイズ}
\index{たるとれつとあれこせいす@タルトレットアレコセイズ}
\index{savory@savory!tartelettesal'ecossaise@tartelettesàl'ecossaise}
\index{tartelettesal'ecossaise@tartelettesàl'ecossaise}


\atoaki{}

#### タルトレット・ド・ハドック {#tartelettes-de-haddock}

<div class="frsubenv">Tartelettes de Haddock</div>

\index{せいうおりー@セイヴォリー!たるとれつととはとつく@タルトレットドハドック}
\index{たるとれつととはとつく@タルトレットドハドック}
\index{savory@savory!tartelettesdehaddock@tartelettesdehaddock}
\index{tartelettesdehaddock@tartelettesdehaddock}


\atoaki{}

#### タルトレット・ア・ラ・フロランタン {#tartelettes-a-la-florentine}

<div class="frsubenv">Tartelettes à la florentine</div>

\index{せいうおりー@セイヴォリー!たるとれつとあらふろらんたん@タルトレットアラフロランタン}
\index{たるとれつとあらふろらんたん@タルトレットアラフロランタン}
\index{savory@savory!tartelettesalaflorentine@tartelettesàlaflorentine}
\index{tartelettesalaflorentine@tartelettesàlaflorentine}


\atoaki{}

#### タルトレット・マルキーズ {#tartelettes-marquise}

<div class="frsubenv">Tartelettes Marquise</div>

\index{せいうおりー@セイヴォリー!たるとれつとまるきーす@タルトレットマルキーズ}
\index{たるとれつとまるきーす@タルトレットマルキーズ}
\index{savory@savory!tartelettesmarquise@tartelettesmarquise}
\index{tartelettesmarquise@tartelettesmarquise}


\atoaki{}

#### タルトレット・ラグラン

<div class="frsubenv">Tartelettes-Raglan</div>

\index{せいうおりー@セイヴォリー!たるとれつとらくらん@タルトレットラグラン}
\index{たるとれつとらくらん@タルトレットラグラン}
\index{savory@savory!tartelettesraglan@tartelettesraglan}
\index{tartelettesraglan@tartelettesraglan}


\atoaki{}

#### タルトレット・トスカ

<div class="frsubenv">Tartelettes-Tosca</div>

\index{せいうおりー@セイヴォリー!たるとれつととすか@タルトレットトスカ}
\index{たるとれつととすか@タルトレットトスカ}
\index{savory@savory!tartelettestosca@tartelettestosca}
\index{tartelettestosca@tartelettestosca}


\atoaki{}

#### タルトレット・ヴァンドーム

<div class="frsubenv">Tartelettes-Vandome</div>

\index{せいうおりー@セイヴォリー!たるとれつとうあんとーむ@タルトレットヴァンドーム}
\index{たるとれつとうあんとーむ@タルトレットヴァンドーム}
\index{savory@savory!tartelettesvendome@tartelettesvendôme}
\index{tartelettesvendome@tartelettesvendôme}


\atoaki{}

#### ウェルシュ・レアビット

<div class="frsubenv">Welsh-Rarebit</div>

\index{せいうおりー@セイヴォリー!うえるしゆれあひつと@ウェルシュレアビット}
\index{うえるしゆれあひつと@ウェルシュレアビット}
\index{savory@savory!welshrarebit@welshrarebit}
\index{welshrarebit@welshrarebit}





</div><!--endRecette-->



\index{せいうおりー@セイヴォリー|)}
\index{けいしよく@軽食⇒セイヴォリー|)}
\index{savory@savory|)}
